function activate(op,sb) {

    //First menu
    jQuery('a[id^="op"]').removeClass("active");
    jQuery("#" + op).addClass("active");

    //Second menu
    jQuery('a[id^="sb"]').removeClass("active");
    jQuery("#" + sb).addClass("active");

}

function view_foto(url) {
    $("#content-right .image-canvas img").attr('src', url);
}

function relocalizar() {
    var ciudad = document.getElementById('ciudad');
    var cp = document.getElementById('cp');
    var direccion = document.getElementById('direccion');
    find_address_sw(ciudad.value + ' ' + cp.value + ' ' + direccion.value);
}