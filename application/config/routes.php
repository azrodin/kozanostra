<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'home/find';

/* General Site */
$route['katalog'] = "product/katalog";
$route['katalog/(:any)/(:any)'] = "product/show/$1/$2";
$route['katalog/(:any)'] = "product/katalog/$1";
$route['kontakt'] = "kontakt/map";
$route['about'] = "home/about";
$route['semia'] = "home/semia";
$route['family'] = "home/family";
$route['zdorovie'] = "zdorovie/kozie_moloko";
$route['zdorovie/kozie-miaso'] = "zdorovie/kozie_miaso";
$route['fotki'] = "gallery/foto";
$route['sertifikatu'] = "documentos/sertifikatu";
$route['zivotnue'] = "home/zivotnue";
$route['pasterizator'] = "home/pasterizator";
$route['sitemap.xml'] = "home/sitemap";


/* Admin routes*/
$route['admin'] = "user/login";
$route['admin/user/(:any)'] = "user/$1";
$route['admin/product/(:any)'] = "product/$1";
$route['admin/category/(:any)'] = "category/$1";
$route['admin/image/(:any)'] = "image/$1";
$route['admin/documentos/(:any)'] = "documentos/$1";
$route['admin/gallery/(:any)'] = "gallery/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */