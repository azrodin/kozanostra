<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {
    
    public function __construct() {
        parent::__construct();     
        $this->init_config();
    }
    
    public function index() {
        $this->_data['view'] = 'home';
        echo Modules::run('template/load', $this->_data);
    }
   
    public function about() {
        $this->_data['view'] = 'about';
        $this->_data['title'] = "О Ферме Коза Ностра";
        $this->_data['description'] = "Козы, козье молоко и эко-продукты из козьего молока, луговой выпас";
        echo Modules::run('template/load', $this->_data);
    }
    
     public function semia() {
        $this->_data['view'] = 'semia';
        $this->_data['title'] = "Наша Семья";
        $this->_data['description'] = "Наша Семья. Наши Животные.";
        echo Modules::run('template/load', $this->_data);
    }
    
    public function zivotnue() {
        $this->_data['view'] = 'zivotnue';
        $this->_data['title'] = "Продажа животных, козликов, бычков и других";
        $this->_data['description'] = "Мы продаём элитных козликов на племя. А также коз, бычков... итд";
        echo Modules::run('template/load', $this->_data);
    }
    
    public function pasterizator() {
        $this->_data['view'] = 'pasterizator';
        $this->_data['title'] = "Мобильный пастеризатор и охладитель молока Home&Laue";
        $this->_data['description'] = "Продам мобильный пастеризатор и охладитель молока Home&Laue. Новый.";
        echo Modules::run('template/load', $this->_data);
    }
    
    public function find() {
        $uri = uri_string();
        if ($uri == "aboutfarm.html") { redirect("/about", "refresh", 301); }
        if ($uri == "family.html") { redirect("/fotki", "refresh", 301); }
        if ($uri == "polezno.html") { redirect("/zdorovie", "refresh", 301); }
        if ($uri == "price.html") { redirect("/katalog", "refresh", 301); }
        if ($uri == "contact.html") { redirect("/kontakt", "refresh", 301); }
        if ($uri == "price_animals.html") { redirect("/zivotnue", "refresh", 301); }
        if ($uri == "photogalereya.html") { redirect("/fotki", "refresh", 301); }
        if ($uri == "certifikatu.html") { redirect("/sertifikatu", "refresh", 301); }
        if ($uri == "index.html") { redirect("/", "refresh", 301); }
        
        $this->_data['view'] = '404';
        $this->_data['title'] = "Error 404";
        $this->_data['description'] = "Error 404";
        echo Modules::run('template/load', $this->_data);

    }
    
    public function sitemap() {
        $this->load->view('sitemap');
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private function init_config() {
        $this->load->model($this->_model, "mdl");
        $this->_data['module'] = strtolower(get_class($this));
    }
    
    private $_data = array();
    private $_model = "mdl_home";
    private $_home = "/home";
    
}