<?php header('Content-type: text/xml'); ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

   <url>
      <loc>http://kozanostra.info/</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
   
   <url>
      <loc>http://kozanostra.info/about</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
    
   <url>
      <loc>http://kozanostra.info/zivotnue</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
    
   <url>
      <loc>http://kozanostra.info/fotki</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
   
   <url>
      <loc>http://kozanostra.info/sertifikatu</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
  
   <url>
      <loc>http://kozanostra.info/kontakt</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>

   <url>
      <loc>http://kozanostra.info/zdorovie</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
    
   <url>
      <loc>http://kozanostra.info/katalog</loc>
      <lastmod>2013-04-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1</priority>
   </url>
   
   <?php echo Modules::run("category/sitemap"); ?>
    
</urlset>