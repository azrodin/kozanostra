<h1 class="title">«КОЗА НОСТРА» - Контакты.</h1>
<script type="text/javascript">
    var map;
    var kozanostra = new google.maps.LatLng(56.534625,37.677956);
      
    function set_marker() {
        var marker = new google.maps.Marker({
                        position: kozanostra,
                        map: map,
                        draggable: false
        });
        var infowindow = new google.maps.InfoWindow({
                content: "<h2>«КОЗА НОСТРА»</h2><p>Московская область, Талдомский район, деревня Головково-Марьино</p>",
                maxWidth: 200
        });

        google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
        });
    }
      
    function initialize() {
        var mapOptions = {
          zoom: 16,
          center: kozanostra,
          mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById('map-koordinatu'), mapOptions);
        set_marker();
      }
      
      google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="map-koordinatu" class="border-5">
</div>
<p>Адрес: Московская область, Талдомский район, деревня Головково-Марьино.</p>