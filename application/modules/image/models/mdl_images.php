<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_images extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Europe/Moscow");
        $this->_path = $_SERVER['DOCUMENT_ROOT'] . '/' . 'public/img/catalogue/';
    }
    
    public function get_table() {
        return $this->_table;
    }
    
    public function get_by_id($id) {
        $this->db->where("id", $id);
        return $this->db->get($this->_table);
    }
    
    public function get_by_slug($slug) {
        $this->db->where("slug", $slug);
        return $this->db->get($this->_table);
    }
    
    public function get_by_product_id($product_id) {
        $this->db->where("product_id", $product_id);
        return $this->db->get($this->_table);
    }
    
    public function get_all() {
        return $this->db->get($this->_table);
    }
    
    public function delete_by_id($id) {
        $this->db->where("id", $id);
        $this->db->delete($this->_table);
    }
    
    public function delete_by_slug($slug) {
        $this->db->where("slug", $slug);
        $this->db->delete($this->_table);
    }
    
    public function delete_by_product_id($product_id) {
        $products = $this->get_by_product_id($product_id);
        foreach($products->result() as $product) {
            $this->delete_by_id($product->id);
            @unlink("{$this->_path}{$product->file}");
            @unlink("{$this->_path}low-{$product->file}");
        }
    }
    
    public function insert($array_data) {
        unset($array_data['id']);
        $array_data["inserted_date"] = date('Y-m-d H:i:s');
        $array_data["updated_date"] = date('Y-m-d H:i:s');
        $this->db->insert($this->_table, $array_data);
        return $this->db->insert_id();
    }
    
     public function upload($product_id) {
        
        $file = my_img_title($_FILES['userfile']['name']);
        $file = time() . '-' . $file;
        
        $config['upload_path'] = $this->_path;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['file_name'] = $file;
        $config['max_size'] = 500;   //Kilobytes
        $config['max_width'] = 800;
        $config['max_height'] = 800;
        $config['overwrite'] = true;
        $config['max_filename'] = 100;
        
	$this->load->library('upload', $config);

        if ($this->upload->do_upload()) {
            if ($this->resize(300, $file, $this->_path)) {
                $array_data = array(
                    'product_id' => $product_id,
                    'file' => $file
                );
                $this->delete_by_product_id($product_id);
                $this->insert($array_data);
            } else {
                return false;
            }
            return true;
        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors("<p>", "</p>"));
            return false;
        }
    }
    
    public function resize($width, $name, $path) {

        $config['image_library'] = 'gd2';
        $config['source_image']	= "{$path}{$name}";
        $config['new_image']	= "{$path}low-{$name}";
        $config['maintain_ratio'] = TRUE;
        $config['width']  = $width;
        $config['height'] = $width;
        $this->load->library('image_lib', $config); 
        if ($this->image_lib->resize()) {
            return true;
        } else {
            $this->session->set_flashdata('error', $this->image_lib->display_errors());
            return false;
        }
    }
    
    public function update($array_data) {
        $this->db->where('id', $array_data['id']);
        $array_data["updated_date"] = date('Y-m-d H:i:s');
        unset($array_data['id']);
        $this->db->update($this->_table, $array_data);
    }
    
    /* SHOULD BE MODIFIED */
    /* !!!! SHOULD USE MY_URL_TITLE ¡¡¡¡ */
    private $_table = "images";
    private $_path;
    
}