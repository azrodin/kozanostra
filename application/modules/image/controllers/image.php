<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function echo_file_by_product_id($product_id) {
        $products = $this->mdl_images->get_by_product_id($product_id);
        if ($products->num_rows == 0) {
            return "no-image.png";
        } else {
            $product = array_pop($products->result());
            return $product->file;
        }
    }
    
    public function all() {
        Modules::run("user/is_logged_in");
        $this->_data['all'] = $this->get_all();
        $this->_data['view'] = 'all'; 
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function form($product_id) {
        Modules::run("user/is_logged_in");
        $this->_data['product_id'] = $product_id;
        $this->_data['view'] = 'form';
        echo Modules::run("template/admin", $this->_data);
    }
    
    public function upload() {
       Modules::run("user/is_logged_in");
       $product_id = $this->input->post('product_id');
       if ($this->validate()) {
           if ($this->mdl_images->upload($product_id)) {
               redirect("/admin/image/form/{$product_id}", "refresh");
           } else {
               redirect("/admin/image/form/{$product_id}", "refresh");
           }
       } else {
           $this->session->set_flashdata('error', validation_errors("<p>", "</p>"));
           redirect("/admin/image/form/{$product_id}", "refresh");
       }  
    }
    
    public function delete($product_id) {
        Modules::run("user/is_logged_in");
        $this->delete_by_product_id($product_id);
        redirect($this->_home, "refresh");
    }
    
    public function delete_by_product_id($product_id) {
        $this->mdl_images->delete_by_product_id($product_id);
    }
    
    /* PRIVATE FUNCTIONS */
    
    private function get_by_id($id) {
        return $this->mdl_images->get_by_id($id);
    }
    
    private function get_all() {
       return $this->mdl_images->get_all();
    }
    
    private function get_by_slug($slug) {
        return $this->mdl_images->get_by_slug($slug);
    }
    
    
    /* SHOULD TO BE MODIFIED */
    private function validate() {
         $field = "file";
         $table = $this->mdl_images->get_table();
         $this->load->library('form_validation');
         $this->form_validation->set_rules('product_id', 'Product ID', "required");
         if ($this->form_validation->run()) {
            return true;
        } else {
            return false;
        }
    }
    
    private function init_config() {
        $this->load->model("mdl_images");
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['submenu'] = "product/submenu";
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private $_data = array();
    private $_home = "/admin/image/all";
    
}