<h1>Show All</h1>
<table>
    <thead>
        <tr>
            <th>ID: </th>
            <th>Product Image: </th>
            <th>File: </th>
            <th>Product: </th>
            <th>Control: </th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($all->result() as $object) { ?>
        <tr>
            <td><?php echo $object->id; ?></td>
            <td>
                <?php $image = Modules::run("image/echo_file_by_product_id", $object->id); ?>
                <p class="img-lista">
                    <a href="<?php echo base_url("/admin/image/form/{$object->id}"); ?>" alt="Edit Image"><img src="<?php echo base_url("public/img/catalogue/low-{$object->file}"); ?>" alt="Image Logo"/></a>
                </p>
            </td>
            <td><?php echo $object->file; ?></td>
            <td><?php echo Modules::run("product/echo_name_by_product_id", $object->product_id); ?></td>
            <td class="control_panel">
                <ul>
                    <li>
                        <a href="<?php echo site_url('admin/image/form/'. $object->product_id); ?>" title="Edit Image"><img src="<?php echo base_url("public/img/pic.png");  ?>" alt="Edit" /></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/image/delete/'. $object->product_id); ?>" title="Delete"><img src="<?php echo base_url("public/img/delete.png"); ?>" alt="Delete" /></a>
                    </li>
                </ul>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>