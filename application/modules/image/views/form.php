<h1>Изменить изображение</h1>
<div id="content-left">
    <div class="error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <div class="upload-form">
        <?php echo form_open_multipart('image/upload') ?>
        <p><input type="file" name="userfile" size="20" /></p>
        <p>
            <input type="hidden" name="product_id" value="<?php echo set_value('product_id', $product_id); ?>" />
        </p>
        <p class="green"><button type="submit">Отправить</button></p>
        </form>
    </div>
    <p style="text-align:center;"><strong><em style="color:red">*</em> Максимальный размер изображения 800x800 и 500 килобайт.</strong></p>
</div>

<div id="content-right">
    <?php $image = Modules::run("image/echo_file_by_product_id", $product_id); ?>
    <p class="image-canvas">
        <img src="<?php echo base_url("public/img/catalogue/low-{$image}"); ?>" alt="No image"/>    
    </p>
</div>