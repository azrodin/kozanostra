<h1 class="title">«КОЗА НОСТРА» - Контакты.</h1>

<p style="text-align:center;">По вопросам реализации в Москве:<br />
     <strong>8 (910) 462-26-42</strong><br /> (Сергей)</p>

<p style="text-align: center;">По вопросам реализации на ферме:<br />
    <strong>8 (916) 998-59-96</strong><br /> (Клава)</p>


<script type="text/javascript">
    var map;
    var kozanostra = new google.maps.LatLng(56.535459,37.673922);
      
    function set_marker() {
        var marker = new google.maps.Marker({
                        position: kozanostra,
                        map: map,
                        draggable: false
        });
        var infowindow = new google.maps.InfoWindow({
                content: "<h2 style=\"font-size: 12px;text-align: center;\">«КОЗА НОСТРА»</h2><p style=\"font-size: 10px; text-align: center;\">Московская область, Талдомский район, деревня Головково-Марьино</p>",
                maxWidth: 200
        });

        google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
        });
    }
      
    function initialize() {
        var mapOptions = {
          zoom: 16,
          center: kozanostra,
          mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById('map-koordinatu'), mapOptions);
        set_marker();
      }
      
      google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="map-koordinatu" class="border-5">
</div>
<p>Адрес: Московская область, Талдомский район, деревня Головково-Марьино.</p>
<p style="text-align: center;"><strong>56.534625</strong> Широты, <strong>37.677956</strong> Долготы. </p>