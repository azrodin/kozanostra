<p class="widget"><img src="<?php echo base_url("/public/img/template/menu-contact.jpg"); ?>" alt="Контактная информация"/></p>

<div class="post-it">
    <div class="rotate-post-it">
        <h3>Доставка осуществляется по:</h3>
        <ul>
            <li>вт-ник с 9:00 до 22:00</li>
            <li>пят-ца с 9:00 до 22:00</li>
        </ul>
        <p><strong>Чтобы оформить заказ пишите или звоните:</strong></p>
        <ul>
            <li><a href="mailto:ferma@kozanostra.info">ferma@kozanostra.info</a></li>
            <li>+7 (910) 462 26 42 Сергей</li>
        </ul>
    </div>
</div>