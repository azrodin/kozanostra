<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontakt extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function map() {
        $this->_data['view'] = 'kontakt';
        echo Modules::run('template/load', $this->_data);
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private function init_config() {
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['sidebar'] = 'kontakt/sidebar';
    }
    
    private $_data = array();
    private $_home = "/kontakt";
    
}