    <h1>Вход: </h1>
    <div id="login-form">
        <form action="<?php echo base_url("/user/sign_in"); ?>" method="post">
            <p><label> Пользователь: </label><input type="text" id="username" name="username" /></p>
            <p><label> Пароль: </label><input type="password" id="password" name="password" /></p>
            <p><button type="submit" id="submit">Вход</button></p>
        </form>
    </div>
    <div class="error"><?php echo $this->session->flashdata('error'); ?></div>