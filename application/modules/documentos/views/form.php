<h1>Добавить сертификат</h1>
<div id="content-left">
    <div class="error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php echo form_open_multipart('documentos/upload') ?>
    <p class="small"><label for="name">Наименование: </label><input type="text" name="name" id="name" size="20" value=""/></p>
    <div class="upload-form">
        <p><input type="file" name="userfile" size="20" /></p>
        <p class="green"><button type="submit">Отправить</button></p>
        </form>
    </div>
    <p style="text-align:center;"><strong><em style="color:red">*</em> Максимальный размер изображения 1200x1200 и 1 мегабайт.</strong></p>
</div>

<div id="content-right">
    
</div>