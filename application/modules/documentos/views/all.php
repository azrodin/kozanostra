<h1>Show All</h1>
<table>
    <thead>
        <tr>
            <th>ID: </th>
            <th>Product Image: </th>
            <th>File: </th>
            <th>Product: </th>
            <th>Control: </th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($all->result() as $object) { ?>
        <tr>
            <td><?php echo $object->id; ?></td>
            <td>
                <p class="img-lista">
                    <img src="<?php echo base_url("public/img/documentos/low-{$object->file}"); ?>" alt="Image"/>
                </p>
            </td>
            <td><?php echo $object->file; ?></td>
            <td><?php echo $object->name; ?></td>
            <td class="control_panel">
                <ul>
                    <li>
                        <a href="<?php echo site_url('admin/documentos/delete/'. $object->id); ?>" title="Delete"><img src="<?php echo base_url("public/img/delete.png"); ?>" alt="Delete" /></a>
                    </li>
                </ul>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>