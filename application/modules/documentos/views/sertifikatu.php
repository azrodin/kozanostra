<h1 class="title">«Наши сертификаты»</h1>

<?php foreach($objects->result() as $object) { ?>
<p class="cer-image-canvas float-left border-5">
    <a rel="lightbox[sertifikatu]" href="<?php echo base_url("/public/img/documentos/{$object->file}"); ?>" rel="lightbox" title="<?php echo $object->name; ?>"><img src="<?php echo base_url("/public/img/documentos/low-{$object->file}"); ?>" alt="<?php echo $object->name; ?>"/></a>
    <span class="cer-image-text  border-5">
        <?php echo $object->name; ?>
    </span>
</p>
<?php } ?>