<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documentos extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function sertifikatu() {
        $this->_data['objects'] = $this->mdl_documentos->get_all();
        $this->_data['view'] = 'sertifikatu';
        echo Modules::run('template/load', $this->_data);
    }
    
    public function echo_file_by_product_id($product_id) {
        $products = $this->mdl_documentos->get_by_product_id($product_id);
        if ($products->num_rows == 0) {
            return "no-image.png";
        } else {
            $product = array_pop($products->result());
            return $product->file;
        }
    }
    
    public function all() {
        $this->_data['all'] = $this->get_all();
        $this->_data['view'] = 'all'; 
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function form() {
        $this->_data['view'] = 'form';
        echo Modules::run("template/admin", $this->_data);
    }
    
    public function upload() {
       if ($this->validate()) {
           if ($this->mdl_documentos->upload()) {
               redirect($this->_home, "refresh");
           } else {         
               redirect("/admin/documentos/form", "refresh");
           }
       } else {
           $this->session->set_flashdata('error', validation_errors("<p>", "</p>"));
           redirect("/admin/documentos/form", "refresh");
       }  
    }
    
    public function delete($id) {
        $this->delete_by_id($id);
        redirect($this->_home, "refresh");
    }
    
    /* PRIVATE FUNCTIONS */
    
    private function delete_by_id($id) {
        return $this->mdl_documentos->delete_all_by_id($id);
    }
    
    private function get_all() {
       return $this->mdl_documentos->get_all();
    }
    
    private function get_by_slug($slug) {
        return $this->mdl_documentos->get_by_slug($slug);
    }
    
    
    /* SHOULD TO BE MODIFIED */
    private function validate() {
         $this->load->library('form_validation');
         $this->form_validation->CI =& $this;
         $this->form_validation->set_rules('name', 'Name', "required");
         if ($this->form_validation->run()) {
            return true;
        } else {
            return false;
        }
    }
    
    private function init_config() {
        $this->load->model("mdl_documentos");
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['submenu'] = "documentos/submenu";
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private $_data = array();
    private $_home = "/admin/documentos/all";
    
}