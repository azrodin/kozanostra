<div id="header-inside-left">
    <h1>Clear Admin</h1>
    <p>Administración de la pagina <b>www.kozanostra.info</b></p>
</div>
<div id="header-inside-right">
    <div class="logout">
        <p>Welcome <?php echo $this->session->userdata('username'); ?>! <a href="<?php echo site_url("/admin/user/sign_out"); ?>">Sign out</a></p>
    </div>
</div>