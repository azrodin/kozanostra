<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="utf8">
<head>
	<title>Clear Admin</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en" />
        <link href="<?php echo base_url(); ?>public/css/admin.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/admin.js" ></script>
        <script type="text/javascript">
            var site_url = "<?php echo base_url(); ?>";
            $(document).ready(function() {
                
                activate('op-1','sb-1');
                jQuery('#content tr:odd').css({'background' : '#ddd'});
                jQuery('.light_red').css({'background' : '#fcc'});
                jQuery('.light_green').css({'background' : '#cfc'});
                jQuery('.light_blue').css({'background' : '#ccf'});

            });
        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-38078377-1']);
            _gaq.push(['_trackPageview']);

            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

          </script>
</head>
    <body>
        <div id="center">
            <div id="header">
                <div id="header-inside">
                    <?php $this->load->view("admin/header"); ?>
                </div>
            </div>
            <div id="menu">
                <div id="menu-inside">
                <div id="main-menu">
                        <?php $this->load->view("admin/menu"); ?>
                </div>
                <div id="sub-menu">
                        <?php $this->load->view("$submenu"); ?>
                </div>
                </div>
            </div>
            <div id="content">
                <?php $this->load->view("{$module}/{$view}"); ?>
            </div>
            <div id="footer">
                <div id="footer-inside"><?php $this->load->view("admin/footer"); ?></div>
            </div>
        </div>
    </body>
</html>