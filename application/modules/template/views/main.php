<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<title><?php echo $title; ?></title>
		<meta name="keywords" content="<?php echo $keywords; ?>" />
		<meta name="description" content="<?php echo $description; ?>" />
		<meta name="robots" content="all" />
                
                <!-- Favicons and stuff -->
                <link rel="shortcut icon" href="<?php echo base_url("public/img/ico/favicon.ico"); ?>" type="image/x-icon" />
                <link rel="apple-touch-icon" href="<?php echo base_url("public/img/ico/apple-touch-icon.png"); ?>" />
                <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url("public/img/ico/apple-touch-icon-57x57.png"); ?>" />
                <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url("public/img/ico/apple-touch-icon-60x60.png"); ?>" />
                <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("public/img/ico/apple-touch-icon-72x72.png"); ?>" />
                <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("public/img/ico/apple-touch-icon-76x76.png"); ?>" />
                <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url("public/img/ico/apple-touch-icon-114x114.png"); ?>" />
                <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url("public/img/ico/apple-touch-icon-120x120.png"); ?>" />
                <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url("public/img/ico/apple-touch-icon-144x144.png"); ?>" />
                <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url("public/img/ico/apple-touch-icon-152x152.png"); ?>" />
                
		<link href="<?php echo base_url("public/css/style.css"); ?>" rel="stylesheet" >
                <link href="<?php echo base_url("public/css/lightbox.css"); ?>" rel="stylesheet" >
                <script src="<?php echo base_url("public/js/jquery.min.js"); ?>" type="text/javascript"></script>
                <script src="<?php echo base_url("public/js/jquery.spritely-0.6.js"); ?>" type="text/javascript"></script>
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
                <script src="<?php echo base_url("public/js/lightbox.js"); ?>" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready( function () {
                        /* Clouds stuff */
                        $('#far-clouds').pan({fps: 30, speed: 0.7, dir: 'left', depth: 30});
                        $('#near-clouds').pan({fps: 30, speed: 1, dir: 'left', depth: 70});
                    });
                </script>
                <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-38078377-1']);
                _gaq.push(['_trackPageview']);

                (function() {
                  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();

              </script>
        </head>
<body>
    
<header id="header" ><?php $this->load->view('common/header'); ?></header>

<div id="center-content" class="clear main-width">
    <section id="content" class="float-right">
        <?php $this->load->view("{$module}/{$view}"); ?>
    </section>

    <aside id="sidebar" class="float-left"><?php $this->load->view("{$sidebar}"); ?></aside>
</div>

<!-- Footer Stuff -->
<div id="footer-top" class="clear main-width">
    
</div>
<footer id="footer" class="clear">
    <?php $this->load->view('common/footer'); ?>
</footer>
</body>
</html>
