<div id="far-clouds" class="far-clouds stage"></div>
<div id="near-clouds" class="near-clouds stage"></div>

<div id="header-inside" class="clear main-width">
    <div class="clear header-top">
    </div>
    <nav class="clear header-middle">
        <ul id="main-menu">
            <li><a href="<?php echo base_url("/"); ?>" title="Ферма"><strong>Ферма</strong></a></li>
            <li><a href="<?php echo base_url("/katalog"); ?>" title="Продукты"><strong>Продукты</strong></a></li>
            <li><a href="<?php echo base_url("/zdorovie"); ?>" title="Здоровье"><strong>Здоровье</strong></a></li>
            <li><a href="<?php echo base_url("/kontakt"); ?>" title="Контакты"><strong>Контакты</strong></a></li>
        </ul>
    </nav>
    <div class="clear header-bottom">

    </div>
</div>