<div id="footer-inside" class="clear main-width">
    <div id="footer-content">
        <p><a href="http://www.kozanostra.info">«КОЗА НОСТРА»</a> - это современное частное фермерское хозяйство, расположенное<br /> в экологически чистом Талдомском районе Подмосковья.</p>
        <p>Чтобы оформить заказ пишите:
            <a href="mailto:ferma@kozanostra.info">ferma@kozanostra.info</a> <br /> или звоните <strong>+7 (910) 462 26 42</strong> (Сергей).
        </p>
        <p>&nbsp;</p>
        <p>
            Powered By <a href="http://ellislab.com/codeigniter">Codeigniter</a> and HMVC extension.
        </p>
        <p>
            Разработка сайта <a href="http://www.azrodin.com">A.Rodin</a> по дизайну <a href="http://www.uhovuhe.ru/">А.Блинова</a>
        </p>
        
    </div>
</div>