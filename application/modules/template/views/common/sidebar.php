<p class="widget"><img src="<?php echo base_url("/public/img/template/milk-cheese.jpg"); ?>" alt="Продукция и цены"/></p>

<nav class="widget">
          <ul id="aside-menu" class="no-bullet">
            <li><a href="<?php echo base_url("/about"); ?>">О ферме</a></li>
            <li><a href="<?php echo base_url("/semia"); ?>">Наша Семья</a></li>
            <li><a href="<?php echo base_url("/fotki"); ?>">Фотогалерея</a></li>
            <li><a href="<?php echo base_url("/zivotnue"); ?>">Продажа животных</a></li>
            <li><a href="<?php echo base_url("/sertifikatu"); ?>">Наши сертификаты</a></li>
          </ul>
</nav>

<p class="widget">
    <a rel="lightbox[sertifikatu]" href="<?php echo base_url("/public/img/documentos/1385119089-certificado-lavka-lavka-low.jpg"); ?>" title="Система экологической сертификации LavkaLavka 1C">
        <img src="<?php echo base_url("/public/img/template/certificado-lavka-lavka-logo-low.jpg"); ?>" alt="Система экологической сертификации LavkaLavka 1C">
    </a>
</p>

<!--
<div class="post-it">
    <div class="rotate-post-it">
        <h3>Продаём молочное такси!</h3>
        <p><strong>
                Эффективное и быстрое приготовление молочных смесей, простая доставка молока из молочной к телятнику.
            </strong></p>
        <ul>
            <li><a href="http://kozanostra.info/pasterizator" title="Мобильный пастеризатор">Подробнее...</a></li>
            <li>Тел: 8 910 462 2642 (Сергей)</li>
        </ul>
    </div>
</div>
-->

<div id="bg-video" class="clear">
          <p>&nbsp;</p>
          <div id="bg-video-inside" class="border-5">
            <div id="bg-video-inside-canvas"> <iframe
                src="http://player.vimeo.com/video/43806177"
                webkitallowfullscreen="" mozallowfullscreen=""
                allowfullscreen="" frameborder="0" height="197"
                width="262"></iframe> </div>
          </div>
</div>

