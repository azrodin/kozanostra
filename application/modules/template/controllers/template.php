<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->load($this->_data);
    }
    
    public function load($data) {
        $this->_data = array_merge($this->_data, $data);
        $this->load->view('main', $this->_data);
    }
    
    public function login($data) {
        $this->_data = array_merge($this->_data, $data);
        $this->load->view('login', $this->_data);
    }
    
    public function admin($data) {
        $this->_data = array_merge($this->_data, $data);
        $this->load->view('admin', $this->_data);
    }
    
    private function init_config() {
        $this->_data = array(
            'header' => 'common/header',
            'footer' => 'common/footer',
            'sidebar' => 'common/sidebar',
            'title' => 'КОЗА НОСТРА: купить козу, деревенская еда, экологические продукты, купить экологически чистые продукты, куплю козу',
            'description' => 'купить козу, деревенская еда, экологические продукты, купить экологически чистые продукты, куплю козу',
            'keywords' => 'купить козу, деревенская еда, экологические продукты, купить экологически чистые продукты, куплю козу'
        );
    }
    
    private $_data;
}