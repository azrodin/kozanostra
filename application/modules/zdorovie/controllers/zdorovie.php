<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zdorovie extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function kozie_moloko() {
        $this->_data['view'] = 'kozie-moloko';
        echo Modules::run('template/load', $this->_data);
    }
    
    public function kozie_miaso() {
        $this->_data['view'] = 'kozie-miaso';
        echo Modules::run('template/load', $this->_data);
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private function init_config() {
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['sidebar'] = 'zdorovie/sidebar';
    }
    
    private $_data = array();
    private $_home = "/zdorovie/kozie_moloko";
    
}