<h1 class="title">«Наши фотки»</h1>

<?php foreach($objects->result() as $object) { ?>
<p class="fot-image-canvas float-left border-5">
    <a rel="lightbox[gallery]" href="<?php echo base_url("/public/img/gallery/{$object->file}"); ?>" rel="lightbox" title="<?php echo $object->name; ?>"><img src="<?php echo base_url("/public/img/gallery/low-{$object->file}"); ?>" alt="<?php echo $object->name; ?>"/></a>
    <span class="fot-image-text  border-5">
        <?php echo $object->name; ?>
    </span>
</p>
<?php } ?>