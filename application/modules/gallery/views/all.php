<h1>Show All</h1>
<table>
    <thead>
        <tr>
            <th>ID: </th>
            <th>Фотка: </th>
            <th>Файл: </th>
            <th>Название: </th>
            <th>Контроль: </th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($all->result() as $object) { ?>
        <tr>
            <td><?php echo $object->id; ?></td>
            <td>
                <p class="img-lista">
                    <img src="<?php echo base_url("public/img/gallery/low-{$object->file}"); ?>" alt="Фотка"/>
                </p>
            </td>
            <td><?php echo $object->file; ?></td>
            <td><?php echo $object->name; ?></td>
            <td class="control_panel">
                <ul>
                    <li>
                        <a href="<?php echo site_url('admin/gallery/delete/'. $object->id); ?>" title="Удалить"><img src="<?php echo base_url("public/img/delete.png"); ?>" alt="Удалить" /></a>
                    </li>
                </ul>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>