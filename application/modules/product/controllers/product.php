<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller {
    
    public function __construct() {
        parent::__construct();     
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function sitemap($category_id, $category_slug) {
        $products = $this->mdl_products->get_all_by_category_id($category_id);
        foreach($products->result() as $product) {
            $updated = new DateTime($product->updated_date);
            $fecha = $updated->format('Y-m-d');
            echo "<url>
                <loc>http://kozanostra.info/katalog/{$category_slug}/{$product->slug}</loc>
                <lastmod>{$fecha}</lastmod>
                <changefreq>monthly</changefreq>
                <priority>0.5</priority>
             </url>";
        }
    }
    
    public function katalog($category = '') {
        if ($category == '') {
            $category = Modules::run("category/get_first_category_slug");      
        }
        $category_id = (int) Modules::run("category/get_id_by_slug", $category);
        if ($category_id == -1) {
            redirect("/katalog", "refresh");
        }
        $this->_data['category_name'] = Modules::run("category/get_name_by_slug", $category);
        $this->_data['all'] = $this->mdl_products->get_all_by_category_id($category_id); 
        $this->_data['view'] = 'katalog';
        $this->_data['category_slug'] = $category;
        $this->_data['title'] = $this->_data['category_name'];
        $this->_data['description'] =  $this->_data['category_name'] . ". Продажа в Москве и Московской Области. " . " Чистые Экологические продукты";
        $this->_data['keywords'] = $this->_data['category_name'] . ", Москва, Московская Область, Эко-Продукты";
        echo Modules::run('template/load', $this->_data);
    }
    
    public function all() {
        Modules::run("user/is_logged_in");
        $this->_data['all'] = $this->get_all();
        $this->_data['view'] = 'all'; 
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function echo_name_by_product_id($product_id) {
        $products = $this->mdl_products->get_by_id($product_id);
        if ($products->num_rows == 0) {
            return "Desconocido";
        } else {
            $product = array_pop($products->result());
            return $product->name;
        }
    }
    
    public function show($category, $slug) {
        $product = $this->get_by_slug($slug);
        
        $category_id = (int) Modules::run("category/get_id_by_slug", $category);
       
        if  (($product->num_rows == 0) || ($category_id == -1)) {
            redirect("/katalog", "refresh");
        }
        
        $object = array_pop($product->result());
        $this->_data['object'] = $object;
        $this->_data['view'] = 'show';
        $this->_data['title'] = $object->name;
        $this->_data['description'] =  $object->name . ". Продажа в Москве и Московской Области. " . "Экопродукты и Деревенские продукты";
        $this->_data['keywords'] = $object->name . ", Моква, Московская Область, Эко Продукты, Деревенские Продукты";
        echo Modules::run('template/load', $this->_data);
    }
    
    public function add() {
        Modules::run("user/is_logged_in");
        $this->_data['action'] = "insert";
        $this->_data['id'] = -1;
        $this->_data['categories'] = Modules::run("category/select", -1);  
        $this->populate($array_data = array());
        $this->_data['view'] = 'form'; 
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function delete($id) {
        Modules::run("user/is_logged_in");
        $this->mdl_products->delete_by_id($id);
        Modules::run("image/delete_by_product_id", $id);
        $this->redirect_home();
    }
    
    public function edit($id) {
        Modules::run("user/is_logged_in");
        $this->_data['id'] = (int) $id;
        $this->_data['action'] = "update";
        $this->_data['categories'] = Modules::run("category/select", $id);
        $object = $this->get_by_id($id);
        $this->populate(array_pop($object->result_array())); 
        $this->_data['categories'] = Modules::run("category/select", $this->_data['category_id']);
        $this->_data['view'] = 'form'; 
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function insert() {
       Modules::run("user/is_logged_in");
       if ($this->validate()) {
           $object_data = $this->post_populate();
           if ( method_exists($this->mdl_products, $this->input->post('action')) ) {
                call_user_func_array( array($this->mdl_products, $this->input->post('action')), array($object_data));
           }
           $this->redirect_home();
       } else {
           if ($this->input->post('action') == 'insert') { $this->add(); }
           if ($this->input->post('action') == 'update') { $this->edit($this->input->post('id')); }
       }  
    }
    
    public function check_categories($category_id) {
        $products = $this->mdl_products->get_by_category_id($category_id);
        if ($products->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /* PRIVATE FUNCTIONS */
    
    private function get_by_id($id) {
        return $this->mdl_products->get_by_id($id);
    }
    
    private function get_all() {
       return $this->mdl_products->get_all();
    }
    
    private function get_by_slug($slug) {
        return $this->mdl_products->get_by_slug($slug);
    }
    
    
    /* SHOULD TO BE MODIFIED */
    private function validate() {
         $this->load->library('form_validation');
         $this->form_validation->set_rules('name', 'Наименование', 'required|min_length[10]|max_length[255]|htmlspecialchars');
         $this->form_validation->set_rules('cuantity', 'Количество', "required|min_length[3]|max_length[50]|htmlspecialchars");
         $this->form_validation->set_rules('price', 'Цена', 'required|decimal');
         $this->form_validation->set_rules('delivery', 'Доставка', 'required|decimal');
         $this->form_validation->set_rules('active', 'Активный', 'required');
         $this->form_validation->set_rules('description', 'Описание', 'required|min_length[100]|htmlspecialchars');
         $this->form_validation->set_rules('action', 'Action', 'required');
         $this->form_validation->set_rules('id', 'Id', 'required|numeric');
         $this->form_validation->set_rules('category_id', 'Category', 'required|numeric');
         if ($this->form_validation->run()) {
            return true;
        } else {
            return false;
        }
    }
    
    private function populate($array_data) {
        if (empty($array_data)) {
            $array_data = array(
                    'name' => '',
                    'active' => 'no',
                    'slug' => '',
                    'price' => 0.00,
                    'delivery' => 0.00,
                    'cuantity' => 0,
                    'description' => ""
            );
        }
        $this->_data = array_merge($array_data, $this->_data);
    }
    
    private function post_populate() {
        return $array = array(
               'id' => $this->input->post('id'),
               'active' => $this->input->post('active'),
               'slug' => my_url_title($this->input->post('name')),
               'price' => $this->input->post('price'),
               'delivery' => $this->input->post('delivery'),
               'name' => $this->input->post('name'),
               'cuantity' => $this->input->post('cuantity'),
               'category_id' => $this->input->post('category_id'),
               'description'=> $this->input->post('description')
        );
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private function init_config() {
        $this->load->model("mdl_products");
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['sidebar'] = 'product/sidebar';
        $this->_data['submenu'] = 'product/submenu';
    }
    
    private $_data = array();
    private $_home = "/product/all";
    
}