<p class="widget"><img src="<?php echo base_url("/public/img/template/products.jpg"); ?>" alt="Продукция и цены"/></p>

<?php echo Modules::run("category/menu_categories"); ?>

<div class="post-it">
    <div class="rotate-post-it">
        <h3>Доставка осуществляется по:</h3>
        <ul>
            <li>вт-ник с 9:00 до 22:00</li>
            <li>пят-ца с 9:00 до 22:00</li>
        </ul>
        <p><strong>Чтобы оформить заказ пишите или звоните:</strong></p>
        <ul>
            <li><a href="mailto:ferma@kozanostra.info">ferma@kozanostra.info</a></li>
            <li>+7 (910) 462 26 42 Сергей</li>
        </ul>
    </div>
</div>

<p class="widget">
    <a rel="lightbox[sertifikatu]" href="<?php echo base_url("/public/img/documentos/1385119089-certificado-lavka-lavka-low.jpg"); ?>" title="Система экологической сертификации LavkaLavka 1C">
        <img src="<?php echo base_url("/public/img/template/certificado-lavka-lavka-logo-low.jpg"); ?>" alt="Система экологической сертификации LavkaLavka 1C">
    </a>
</p>