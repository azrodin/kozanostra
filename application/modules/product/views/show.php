<?php
    $inserted = new DateTime($object->inserted_date);
    $updated = new DateTime($object->updated_date);
?>

<h1 class="title"><?php echo $object->name; ?></h1>

<div id="hires-canvas">
     <?php $image = Modules::run("image/echo_file_by_product_id", $object->id); ?>
    <img src="<?php echo base_url("/public/img/catalogue/{$image}"); ?>" alt="<?php echo $object->name; ?>"/>
</div>

<ul id="product-details">
    <li><span>Количество </span> <?php echo $object->cuantity; ?></li>
    <li><span>Самовывоз с фермы </span> <?php echo $object->price; ?> руб</li>
    <li><span>С доставкой в Москву </span> <?php echo $object->delivery; ?> руб</li>
</ul>

<p style="padding: 10px 0px;">
    <?php echo $object->description; ?>
</p>