<script type="text/javascript">
    $(function() {
        $('#content tr:odd').css({'background' : '#eee'});
        $('#content .image-canvas-text').fadeTo('slow', 0.85);
        $('#product-list .text-product-canvas p, #product-list .text-product-canvas, #product-list .image-product-canvas').hover(function() {
            $(this).css({'border-color' : '#000'});
        }, function() {
            $(this).css({'border-color' : '#aaa'});
        });
    });
</script>
<h1 class="title" style="text-align: center;">«<?php echo $category_name; ?>»</h1>
<table id="product-list" cellspacing="10">
    <tbody>
        <?php foreach($all->result() as $object) { ?>
             <?php $image = Modules::run("image/echo_file_by_product_id", $object->id); ?>
             <?php $link = "/katalog/{$category_slug}/{$object->slug}"; ?>
            <tr>
                <td class="image-product-canvas border-5">
                    <p class="price-canvas">
                         <span><?php echo $object->price; ?> руб.</span>
                    </p>
                     <a href="<?php echo base_url($link); ?>" title="<?php echo $object->name; ?>">
                        <img src="<?php echo base_url("/public/img/catalogue/low-{$image}"); ?>" alt="<?php echo $object->name; ?>"/>
                    </a>
                </td>
                <td class="text-product-canvas border-5" valign="top">
                    <h2>
                        <a href="<?php echo base_url($link); ?>" title="<?php echo $object->name; ?>">
                            <em><?php echo $object->name; ?> </em>
                        </a>
                        
                    </h2>
                    <h3>Количество <strong><?php echo $object->cuantity; ?></strong></h3>
                    <p>
                    <?php echo word_limiter($object->description, 20); ?> <a href="<?php echo base_url($link); ?>" title="<?php echo $object->name; ?>">[+info]</a>
                    </p>
            </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<h3 style="text-align: center;">
        Мы дорого ценим свой труд и много вкладываем в поддержание качества продукции. Покупая товары фермы «Коза ностра» вы можете быть уверены в том, что:
</h3>
<ol style="margin: 10px 60px;">
        <li style="margin-bottom: 15px;">В отличие от промышленных фабрик, <strong>на нашей ферме не используются ускорители роста, антибиотики, гормоносодержащие стимуляторы, красители, консерванты и никакие химические корма.</strong>
сбалансированный рацион питания животных и птиц. В его состав входит клевер, овёс, кукуруза, морковь, картошка, свекла, выращенные на огороде нашей же фермы, а также комбикорм, производства Курского комбината хлебопродуктов, сделанный по нашему заказу..</li>
        <li style="margin-bottom: 15px;">
            Деятельность нашей фермы находится под регулярным контролем профессионального ветеринарного врача (д-р Истраткин Олег Валерьевич).
животные и птицы находятся в цивилизованных условиях. <strong>На нашей ферме нет места тесноте и грязи.</strong>
        </li>
</ol>

<p>* <strong>Минимальная сумма заказа: 3000 руб. Место доставки по согласованию с Сергеем или Клавой по телефонам, <a href="<?php echo base_url("/kontakt"); ?>">указанным на сайте</a>.</strong></p>