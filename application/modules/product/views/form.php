<div id="content-left">
    <h1><?php echo ucwords($action); ?></h1>
    <div class="error"><?php echo validation_errors('<p>', '</p>') ?></div>
    <form action="<?php echo site_url("/product/insert"); ?>" method="post">
        <p class="small"><label for="name">Наименование: </label> <input type="text" id="name" name="name" value="<?php echo set_value('name', $name); ?>" /></p>
        <p class="small"><label for="category_id">Категория: </label> <?php echo $categories; ?></p>
        <p class="small"><label for="price">Цена: </label> <input type="text" id="price" name="price" value="<?php echo number_format(set_value('price', $price), 2); ?>" /></p>
        <p class="small"><label for="delivery">Цена с доставкой: </label> <input type="text" id="delivery" name="delivery" value="<?php echo number_format(set_value('delivery', $delivery), 2, '.', ''); ?>" /></p>
        <p class="small"><label for="cuantity">Количество: </label> <input type="text" id="cuantity" name="cuantity" value="<?php echo set_value('cuantity', $cuantity); ?>" /></p>
        <p class="small"><label>Описание: </label><textarea name="description" ><?php echo set_value('description', $description); ?></textarea></p>
        <p class="small">
            <label for="active">Активный?: </label>
            <select name="active">
                <option value="yes" <?php if($active == "yes") echo "selected"; ?>>Да</option>
                <option value="no" <?php if($active == "no") echo "selected"; ?>>Нет</option>
            </select>
        </p>
        <p>
            <input type="hidden" name="action" value="<?php echo set_value('action', $action); ?>" />
            <input type="hidden" name="id" value="<?php echo set_value('id',$id); ?>" />
        </p>
        <p class="green"><button type="submit"><?php echo ucwords($action); ?></button></p>
    </form>    
</div>

<div id="content-right">
    <h1>Picture</h1>
    <div class="text-canvas">
            <?php $image = Modules::run("image/echo_file_by_product_id", $id); ?>
            <p>
                        <img style="border: 2px solid #666;" src="<?php echo base_url("public/img/catalogue/low-{$image}"); ?>" alt="No Image"/>
                        <br />
                        <small>Фотка. Можно изменить только если продукт уже добавлен.</small>
                </p>

                
        
    </div>
</div>

