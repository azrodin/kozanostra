<h1>Show All</h1>
<table>
    <thead>
        <tr>
            <th>Active: </th>
            <th>ID: </th>
            <th>Изображение: </th>
            <th>Наименование: </th>
            <th>Категория: </th>
            <th>Цена: </th>
            <th>Доставка: </th>
            <th>Контроль: </th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($all->result() as $object) { ?>
        <tr id="<?php echo "product-{$object->id}" ?>" >
            <td><span <?php if ($object->active == "no") echo "style='color: red; font-weight: bold;'"; ?>><?php echo $object->active; ?><span></td>
            <td><?php echo $object->id; ?></td>
            <td>
                <?php $image = Modules::run("image/echo_file_by_product_id", $object->id); ?>
                <p class="img-lista">
                    <a href="<?php echo base_url("/admin/image/form/{$object->id}"); ?>" alt="Поменять картинку"><img src="<?php echo base_url("public/img/catalogue/low-{$image}"); ?>" alt="Логотип"/></a>
                </p>
            </td>
            <td><?php echo $object->name; ?></td>
            <td><?php echo Modules::run("category/get_name", $object->category_id); ?></td>
            <td><?php echo number_format($object->price, 2); ?></td>
            <td><?php echo number_format($object->delivery, 2); ?></td>
            <td class="control_panel">
                <ul>
                    <li>
                        <a href="<?php echo site_url('admin/product/edit/'. $object->id); ?>" title="Редактировать"><img src="<?php echo base_url("public/img/edit.png");  ?>" alt="Редактировать" /></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/image/form/'. $object->id); ?>" title="Поменять картинку"><img src="<?php echo base_url("public/img/pic.png");  ?>" alt="Поменять картинку" /></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/product/delete/'. $object->id); ?>" title="Удалить продукт"><img src="<?php echo base_url("public/img/delete.png"); ?>" alt="Удалить продукт" /></a>
                    </li>
                </ul>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>