<div class="error"><?php echo $this->session->flashdata('error'); ?></div>
<h1>Show All</h1>
<table>
    <thead>
        <tr>
            <th>ID: </th>
            <th>Сортировка: </th>
            <th>Наименование: </th>
            <th>Контроль: </th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($all->result() as $object) { ?>
        <tr>
            <td><?php echo $object->id; ?></td>
            <td><?php echo $object->sort; ?></td>
            <td><?php echo $object->name; ?></td>
            <td class="control_panel">
                <ul>
                    <li>
                        <a href="<?php echo site_url('admin/category/edit/'. $object->id); ?>" title="Редактировать"><img src="<?php echo base_url("public/img/edit.png");  ?>" alt="Редактировать" /></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/category/delete/'. $object->id); ?>" title="Удалить"><img src="<?php echo base_url("public/img/delete.png"); ?>" alt="Удалить" /></a>
                    </li>
                </ul>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>