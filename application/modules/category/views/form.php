<h1><?php echo ucwords($action); ?></h1>
<div class="error"><?php echo validation_errors('<p>', '</p>') ?></div>
<form action="<?php echo site_url("/admin/category/insert"); ?>" method="post">
    <p class="small"><label for="name">Наименование: </label> <input type="text" id="name" name="name" value="<?php echo set_value('name', $name); ?>" /></p>
    <p class="small"><label for="sort">Сортировка: </label> <input type="text" id="sort" name="sort" value="<?php echo set_value('sort', $sort); ?>" /></p>
    <p>
        <input type="hidden" name="action" value="<?php echo set_value('action', $action); ?>" />
        <input type="hidden" name="id" value="<?php echo set_value('id',$id); ?>" />
    </p>
    <p class="green"><button type="submit"><?php echo ucwords($action); ?></button></p>
</form>