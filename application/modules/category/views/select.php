<select id="category_id" name="category_id">
    <option value="-1">Не указано</option>
    <?php foreach($categories->result() as $cat) { ?>
        <option <?php if ($selected_category == $cat->id) { echo "selected"; } ?> value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
    <?php } ?>
</select>