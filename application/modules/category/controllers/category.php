<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->init_config();
    }
    
    public function index() {
        $this->redirect_home();
    }
    
    public function all() {
        Modules::run("user/is_logged_in");
        $this->_data['all'] = $this->get_all();
        $this->_data['view'] = 'all';
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function sitemap() {
        $categories = $this->get_all();      
        foreach($categories->result() as $category) {
            $updated = new DateTime($category->updated_date);
            $fecha = $updated->format('Y-m-d');
            echo "<url>
                <loc>http://kozanostra.info/katalog/{$category->slug}</loc>
                <lastmod>{$fecha}</lastmod>
                <changefreq>monthly</changefreq>
                <priority>0.8</priority>
             </url>";
            echo Modules::run("product/sitemap", $category->id, $category->slug);
        }
    }
    
    public function menu_categories() {
        $this->_data['objects'] = $this->get_all();
        $this->load->view("menu-categories", $this->_data);
    }
    
    public function select($id) {
        $this->_data['categories'] = $this->get_all();
        $this->_data['selected_category'] = $id;
        $this->load->view('select', $this->_data);
    }
    
    public function add() {
        Modules::run("user/is_logged_in");
        $this->_data['action'] = "insert";
        $this->_data['id'] = -1;
        $this->populate($array_data = array());
        $this->_data['view'] = 'form';
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function delete($id) {
        Modules::run("user/is_logged_in");
        if(Modules::run("product/check_categories", $id)) {
            $this->session->set_flashdata('error', "<p>Категория успешна удалена</p>");
            $this->mdl_categories->delete_by_id($id);
            redirect($this->_home, "refresh");
        } else {
            $this->session->set_flashdata('error', "<p>Данная категория имеет товары, нужно сначала удалить продукты или поменять их категорию</p>");
            redirect($this->_home, "refresh");
        }
    }
    
    public function edit($id) {
        Modules::run("user/is_logged_in");
        $this->_data['id'] = (int) $id;
        $this->_data['action'] = "update";
        $object = $this->get_by_id($id);
        $this->populate(array_pop($object->result_array())); 
        $this->_data['view'] = 'form';
        echo Modules::run('template/admin', $this->_data);
    }
    
    public function insert() {
        Modules::run("user/is_logged_in");
       if ($this->validate()) {
           $object_data = $this->post_populate();
           if ( method_exists($this->mdl_categories, $this->input->post('action')) ) {
                call_user_func_array( array($this->mdl_categories, $this->input->post('action')), array($object_data));
           }
           $this->redirect_home();
       } else {
           if ($this->input->post('action') == 'insert') { $this->add(); }
           if ($this->input->post('action') == 'update') { $this->edit($this->input->post('id')); }
       }  
    }
    
    public function get_name($id) {
        if ($id != -1) {
            $category = $this->mdl_categories->get_by_id($id);
            $category = array_pop($category->result());
            echo $category->name;
        } else {
            echo "Не указано";
        }
    }
    
    public function get_id_by_slug($slug) {
        $categories = $this->get_by_slug($slug);
        if ($categories->num_rows != 0) {
            $category = array_pop($categories->result());
            return $category->id;
        } else {
            return -1;
        }
    }
    
    public function get_name_by_slug($slug) {
        $categories = $this->get_by_slug($slug);
        $category = array_pop($categories->result());
        return $category->name;
    }
    
    public function get_first_category_slug() {
        $categories = $this->mdl_categories->get_first();
        $category = array_pop($categories->result());
        return $category->slug;
    }
    
    /* PRIVATE FUNCTIONS */
    
    private function get_by_id($id) {
        return $this->mdl_categories->get_by_id($id);
    }
    
    private function get_all() {
       return $this->mdl_categories->get_all();
    }
    
    private function get_by_slug($slug) {
        return $this->mdl_categories->get_by_slug($slug);
    }
    
    
    /* SHOULD TO BE MODIFIED */
    private function validate() {
         $this->load->library('form_validation');
         $this->form_validation->set_rules('name', 'Наименование', 'required|max_length[50]|min_length[3]');
         $this->form_validation->set_rules('sort', 'Сортировка', 'required|is_natural|less_than[99]');
         $this->form_validation->set_rules('slug', 'Slug', "requiredmax_length[50]|min_length[3]");
         $this->form_validation->set_rules('action', 'Action', 'required');
         $this->form_validation->set_rules('id', 'Id', 'required|numeric');
         if ($this->form_validation->run()) {
            return true;
        } else {
            return false;
        }
    }
    
    private function populate($array_data) {
        if (empty($array_data)) {
            $array_data = array(
                    'name' => 'name',
                    'sort' => 0,
                    'slug' => 'Slug'
            );
        }
        $this->_data = array_merge($array_data, $this->_data);
    }
    
    private function post_populate() {
        return $array = array(
               'id' => $this->input->post('id'),
               'slug' => my_url_title($this->input->post('name')),
               'name' => $this->input->post('name'),
               'sort' => $this->input->post('sort')
        );
    }
    
    private function redirect_home() {
        redirect("{$this->_home}", "refresh");
    }
    
    private function init_config() {
        $this->load->model("mdl_categories");
        $this->_data['module'] = strtolower(get_class($this));
        $this->_data['submenu'] = 'product/submenu';
    }
    
    private $_data = array();
    private $_home = "/category/all";
    
}