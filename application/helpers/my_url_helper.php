<?php

if ( !defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Function My URL Title.
 */
if ( ! function_exists('my_url_title') ) {

    function my_url_title($string) {   
        $string = transliteration_rus_lat($string);
        $string = str_replace(' ', '-', $string);
        $devolver = strtolower($string);
        $b     = array("á","é","í","ó","ú","ä","ë","ï","ö","ü","à","è","ì","ò","ù","ñ"," ",",",".",";",":","¡","!","¿","?",'"');
        $c     = array("a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","n","","","","","","","","","",'');
        $devolver = str_replace($b,$c,$devolver);

        return url_title($devolver);
    }

}

/**
 * Function My URL Title.
 */
if ( ! function_exists('my_img_title') ) {

    function my_img_title($string) {
        $string = transliteration_rus_lat($string);
        $string = str_replace(' ', '-', $string);
        $devolver = strtolower($string);
        $b     = array("á","é","í","ó","ú","ä","ë","ï","ö","ü","à","è","ì","ò","ù","ñ"," ",",",";",":","¡","!","¿","?",'"');
        $c     = array("a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","n","","","","","","","",'');
        $devolver = str_replace($b,$c,$devolver);

        return $devolver;
    }

}


/**
 * ¿Delete this shit? Dont remember why I put this one!
 */
if ( ! function_exists('url_exists') ) {
    function url_exists($url) { 
        $hdrs = @get_headers($url); 
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false; 
    } 
}

/*
 * Transliteration RUSSIAN - CIRILIC
 */
if ( ! function_exists('transliteration_rus_lat') ) {
    function transliteration_rus_lat($text) { 
            $cyr  = array('а','б','в','г','д','e','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у', 
            'ф','х','ц','ч','ш','щ','ъ', 'ы','ь', 'э', 'ю','я','А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У',
            'Ф','Х','Ц','Ч','Ш','Щ','Ъ', 'Ы','Ь', 'Э', 'Ю','Я' );
            $lat = array( 'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p','r','s','t','u',
            'f' ,'h' ,'ts' ,'ch','sh' ,'sht' ,'a', 'i', 'y', 'e' ,'yu' ,'ya','A','B','V','G','D','E','Zh',
            'Z','I','Y','K','L','M','N','O','P','R','S','T','U',
            'F' ,'H' ,'Ts' ,'Ch','Sh' ,'Sht' ,'A' ,'Y' ,'Yu' ,'Ya' );
            return str_replace($cyr, $lat, $text);
    }
}