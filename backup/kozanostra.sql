-- phpMyAdmin SQL Dump
-- version 3.4.10.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-01-2014 a las 17:57:19
-- Versión del servidor: 5.1.61
-- Versión de PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `kozanostra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(2) NOT NULL DEFAULT '0',
  `inserted_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `slug` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `sort`, `inserted_date`, `updated_date`, `slug`, `name`) VALUES
(5, 1, '2013-04-10', '2013-04-15', 'koziy-sir', 'Козий сыр'),
(6, 6, '2013-04-11', '2013-11-22', 'myasni-produkti', 'Мясные продукты'),
(7, 5, '2013-04-11', '2013-11-22', 'molochni-produkti-iz-korovygo-moloka', 'Молочные продукты из Коровьего Молока'),
(9, 4, '2013-04-15', '2013-11-22', 'korovy-moloko', 'Коровье Молоко'),
(10, 2, '2013-04-18', '2013-04-18', 'kozy-moloko', 'Козье молоко'),
(11, 3, '2013-04-18', '2013-04-18', 'molochni-produkti-iz-kozygo-moloka', 'Молочные продукты из Козьего Молока');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inserted_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file` (`file`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `inserted_date`, `updated_date`, `file`, `name`) VALUES
(6, '2013-04-16 18:56:52', '2013-04-16 18:56:52', '1366124212-koziol-sultan.jpg', 'Племенное свидетельство на нашего нубийского козла Султана.'),
(7, '2013-04-16 18:58:20', '2013-04-16 18:58:20', '1366124300-camamber.jpg', 'Анализ сыра типа camamber из козьего молока.'),
(8, '2013-04-18 13:04:22', '2013-04-18 13:04:22', '1366275862-chevre-1-1.jpg', 'Анализ сыра типа chevre из смеси козьего и коровьего молока'),
(9, '2013-04-18 13:05:10', '2013-04-18 13:05:10', '1366275910-chevre-koza.jpg', 'Анализ сыра типа chevre из козьего молока'),
(10, '2013-04-18 13:05:58', '2013-04-18 13:05:58', '1366275958-img854.jpg', 'Пример результата одно из Анализов на здоровье коз, которое мы проводили в независимой московской лаборатории. Лицевая сторона.'),
(11, '2013-04-18 13:06:32', '2013-04-18 13:06:32', '1366275991-img855.jpg', 'Пример результата одно из Анализов на здоровьего коз, которое мы проводили в независимой московской лаборатории. Обратная сторона'),
(12, '2013-04-18 13:07:23', '2013-04-18 13:07:23', '1366276042-moloko.jpg', 'Анализ козьего молока.'),
(13, '2013-04-18 13:08:01', '2013-04-18 13:08:01', '1366276081-syrli-1-1.jpg', 'Анализ пресного сыра из смеси козьего и коровьего молока.'),
(14, '2013-04-18 13:09:16', '2013-04-18 13:09:16', '1366276156-syrli.jpg', 'Анализ пресного сыра из козьего молока.'),
(15, '2013-04-18 13:09:55', '2013-04-18 13:09:55', '1366276195-syvorotka.jpg', 'Анализ сыворотки из козьего молока'),
(16, '2013-04-18 13:10:56', '2013-04-18 13:10:56', '1366276256-tvorog.jpg', 'Анализ творога из козьего молока'),
(17, '2013-04-18 13:13:03', '2013-04-18 13:13:03', '1366276382-vet.jpg', 'Пример Ветеринарного свидетельства, которые мы получаем для реализации товара. Лицевая сторона.'),
(18, '2013-04-18 13:13:51', '2013-04-18 13:13:51', '1366276430-vet-2.jpg', 'Пример Ветеринарного свидетельства, которые мы получаем для реализации товара. Обратная сторона.'),
(19, '2013-11-22 14:18:10', '2013-11-22 14:18:10', '1385119089-certificado-lavka-lavka-low.jpg', 'Система экологической сертификации LavkaLavka 1C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inserted_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file` (`file`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`id`, `inserted_date`, `updated_date`, `file`, `name`) VALUES
(5, '2013-04-18 13:38:55', '2013-04-18 13:38:55', '1366277935-kozanostra_goats.jpg', 'Козочки'),
(4, '2013-04-18 13:38:35', '2013-04-18 13:38:35', '1366277915-kozanostra_chiken.jpg', 'Куры'),
(6, '2013-04-18 13:39:26', '2013-04-18 13:39:26', '1366277966-kozanostra_goats_2.jpg', 'Козочки'),
(7, '2013-04-18 13:40:33', '2013-04-18 13:40:33', '1366278033-krasim.jpg', 'Красим дом'),
(8, '2013-04-18 13:43:54', '2013-04-18 13:43:54', '1366278234-nasha-semia.jpg', 'Наша семья'),
(9, '2013-04-18 13:50:56', '2013-04-18 13:50:56', '1366278656-kozel-v-snegu.jpg', 'Султан на снегу'),
(10, '2013-04-19 15:19:39', '2013-04-19 15:19:39', '1366370378-assoll-2.jpg', 'Ассоль'),
(14, '2013-04-29 15:11:49', '2013-04-29 15:11:49', '1367233909-milka-nasha-korova.jpg', 'Наша Корова'),
(12, '2013-04-19 15:27:49', '2013-04-19 15:27:49', '1366370869-kozliata.jpg', 'козлики'),
(13, '2013-04-19 15:34:30', '2013-04-19 15:34:30', '1366371269-novui-sur.jpg', 'наш новый сыр'),
(15, '2013-05-27 12:32:58', '2013-05-27 12:32:58', '1369643578-doch-sultana-alsu.jpg', 'Дочь Султана Алсу'),
(16, '2013-05-27 12:37:54', '2013-05-27 12:37:54', '1369643874-novaia-devochka-sabah.jpg', 'Новая нубийская девочка Сабах'),
(17, '2013-06-20 11:11:11', '2013-06-20 11:11:11', '1371712271-kozlionok.jpg', 'Родила сегодня коза Наташа двух козлят'),
(19, '2013-06-20 11:19:40', '2013-06-20 11:19:40', '1371712780-kozliata-2.jpg', 'Анфас');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '-1',
  `inserted_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file` (`file`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Volcado de datos para la tabla `images`
--

INSERT INTO `images` (`id`, `product_id`, `inserted_date`, `updated_date`, `file`) VALUES
(33, 12, '2013-04-15 13:55:12', '2013-04-15 13:55:12', '1366019712-moloko-kozie.jpg'),
(20, 11, '2013-04-15 13:01:11', '2013-04-15 13:01:11', '1366016470-sur-miagkii-korova-kozii.jpg'),
(22, 10, '2013-04-15 13:27:43', '2013-04-15 13:27:43', '1366018063-tvorog-koza.jpg'),
(23, 14, '2013-04-15 13:33:29', '2013-04-15 13:33:29', '1366018408-sur-tverdui-kozii.jpg'),
(47, 15, '2013-04-22 13:38:49', '2013-04-22 13:38:49', '1366623529-chevre.jpg'),
(16, 16, '2013-04-12 08:58:49', '2013-04-12 08:58:49', '1365749928-hi-camember.jpg'),
(25, 17, '2013-04-15 13:37:33', '2013-04-15 13:37:33', '1366018653-rikotta-koza.jpg'),
(26, 18, '2013-04-15 13:41:42', '2013-04-15 13:41:42', '1366018901-smetana-koza.jpg'),
(46, 19, '2013-04-22 13:37:41', '2013-04-22 13:37:41', '1366623461-maslo-slivochnoe-kozie.jpg'),
(28, 20, '2013-04-15 13:44:48', '2013-04-15 13:44:48', '1366019088-prostokvasha-koza.jpg'),
(44, 21, '2013-04-22 13:36:13', '2013-04-22 13:36:13', '1366623372-suvorotka-molochnaia.jpg'),
(30, 22, '2013-04-15 13:52:24', '2013-04-15 13:52:24', '1366019544-sur-miagkii-korova-kozii.jpg'),
(31, 23, '2013-04-15 13:53:17', '2013-04-15 13:53:17', '1366019596-sur-tverdui-kozii.jpg'),
(48, 24, '2013-04-22 13:39:17', '2013-04-22 13:39:17', '1366623557-chevre.jpg'),
(34, 25, '2013-04-15 13:56:02', '2013-04-15 13:56:02', '1366019761-moloko-korovie.jpg'),
(35, 26, '2013-04-15 13:58:20', '2013-04-15 13:58:20', '1366019900-moloko-korovie.jpg'),
(36, 27, '2013-04-15 14:00:35', '2013-04-15 14:00:35', '1366020035-tvorog-korova.jpg'),
(38, 29, '2013-04-15 14:02:56', '2013-04-15 14:02:56', '1366020176-prostokvasha-korova.jpg'),
(45, 30, '2013-04-22 13:36:57', '2013-04-22 13:36:57', '1366623417-suvorotka-molochnaia.jpg'),
(40, 31, '2013-04-15 14:04:48', '2013-04-15 14:04:48', '1366020288-iaiza-kurinue.jpg'),
(41, 32, '2013-04-15 14:05:55', '2013-04-15 14:05:55', '1366020355-iaiza-perepelinue.jpg'),
(43, 33, '2013-04-18 15:36:09', '2013-04-18 15:36:09', '1366284969-miaso-kozlikov.jpg'),
(49, 34, '2013-04-22 13:43:29', '2013-04-22 13:43:29', '1366623809-hi-camember.jpg'),
(50, 36, '2013-05-13 11:54:51', '2013-05-13 11:54:51', '1368431690-kuru.jpg'),
(51, 35, '2013-05-13 11:55:39', '2013-05-13 11:55:39', '1368431739-miaso-kozlikov.jpg'),
(53, 37, '2013-12-09 13:10:31', '2013-12-09 13:10:31', '1386583829-queso-aceitunas-2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inserted_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `edad` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `objects`
--

INSERT INTO `objects` (`id`, `inserted_date`, `updated_date`, `slug`, `name`, `edad`) VALUES
(11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'otro slug', 'Name', 1),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ccc', '', 0),
(4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ddd', '', 0),
(12, '2013-03-08 11:18:24', '2013-03-08 11:18:24', 'Slug666', 'Name', 1),
(6, '2013-02-18 16:24:11', '2013-02-18 16:24:11', 'fff', '', 0),
(-1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'rrr', 'asdasd', 77),
(7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'asd', 'asd', 5),
(8, '0000-00-00 00:00:00', '2013-03-08 11:21:27', 'Slug878', 'Name77777', 1),
(9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Slug22', 'Name', 1),
(10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '333', 'Name', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '-1',
  `active` varchar(3) NOT NULL DEFAULT 'yes',
  `inserted_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `delivery` float NOT NULL,
  `cuantity` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `category_id`, `active`, `inserted_date`, `updated_date`, `slug`, `name`, `price`, `delivery`, `cuantity`, `description`) VALUES
(12, 10, 'yes', '2013-04-11', '2013-11-14', 'kozy-moloko-tslyno-iili-pastrizovanno', 'Козье молоко (цельное и/или пастеризованное)', 180, 199, '1 л', 'В стаде имеются 54 дойные козы. Приобретены в племенных хозяйствах, с сертификатами и родословной. Полученное в стаде молоко смешивается и за счёт присутствия в смеси(купаже) молока от элитных коз, продукты из него обладают непревзойдёнными качествами и вкусом. Свежесть продукта непревзойдённая: доставляем в Москву молоко из-под козы и коровы вчерашней дойки! По желанию клиента: или парное или пастеризованное.'),
(11, 5, 'yes', '2013-04-11', '2013-11-14', 'sir-molodoy-prsniy-iz-kozygo-moloka', 'Сыр молодой пресный из козьего молока', 80, 95, '100 гр', 'Этот сыр сделан из чисто козьего экологического молока высокопородных коз. Пасутся наши козы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. Сыр этот идеально подходит для завтрака с ложечкой мёда или варенья.'),
(10, 11, 'yes', '2013-04-11', '2013-11-14', 'tvorog-iz-kozygo-moloka', 'Творог из козьего молока', 75, 85, '100 гр', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком.'),
(14, 5, 'yes', '2013-04-11', '2013-11-14', 'tvrdiy-sir-iz-kozygo-moloka', 'Твердый сыр из козьего молока', 90, 110, '100 гр', 'Авторский сыр из экологического молока элитных коз. Пасутся наши козы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. Для гурманов всех возрастов - идеальное блюдо на завтрак с ложечкой мёда или варенья.'),
(15, 5, 'yes', '2013-04-11', '2013-11-14', 'sir-tipa-chevre-iz-kozygo-moloka', 'Сыр типа Chevre из козьего молока', 75, 90, '100 гр', 'Сыр, сделанный по рецепту, привезенному из Франции, из экологического молока от высокопородистых коз. Пасутся наши козы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. Мягкий молодой Шевре прекрасный завтрак для молодых дам-с. Можно добавить капельку мёда или ложечку варенья.'),
(16, 5, 'yes', '2013-04-12', '2013-11-14', 'sir-s-bloy-plsnyyu-tipa-kamambrproizvoditsya-po-zakazu', 'Сыр с белой плесенью типа камамбер(производится по заказу)', 95, 115, '100 гр', 'Наш сыр, типа камамбер, с белой плесенью, есть предмет особой гордости автора всех молочных продуктов фермы Коза Ностра. Изготавливается он совершенно отдельно от всех остальных сыров и молока вообще, т.к. используемая для его приготовления белая плесень Penicillium Candidum и конкретно её споры могут попасть на другие молочные продукты. Плесень эту мы привозим из Франции. Выдерживается сыр в специальном шкафу при постоянной температуре 12 градусов и влажности 90%. Срок вызревания от 3-х до 4-х недель в зависимости от состава молока. Этот вид сыра рекомендуется дегустировать после обеда с вином и фруктами.'),
(17, 5, 'yes', '2013-04-12', '2013-11-14', 'molodoy-sir-rikotta-iz-kozygo-moloka', 'Молодой сыр рикотта из козьего молока', 60, 75, '100 гр', 'Молодой сыр Рикотта или Рекесон из экологического молока подходит для лёгкого ужина или предзавтрака. Приготавливается по заказу клиента и может иметь во вкусе разные оттенки: с добавлением укропа или базилика, например, или с кусочками различных фруктов и тогда, блюдо подаётся на десерт.'),
(18, 11, 'yes', '2013-04-15', '2013-11-14', 'gusti-slivki', 'Густые сливки', 100, 110, '100 гр', 'Густые сливки из нашего козьего экологического молока редкий по своему качеству продукт. В Москве не найдёте. Выход продукта очень маленький, а потому дорого, но ценно. Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком.'),
(19, 11, 'yes', '2013-04-15', '2013-11-14', 'slivochno-maslo-iz-kozygo-molokaeksklyuziv', 'Сливочное масло из козьего молока(эксклюзив)', 350, 400, '200 гр', 'Сливочного масла из козьего молока Вы не найдёте на прилавках магазинов. магазинов ни дорогих, ни дешевых. Это деликатес и делают его хозяйки коз только для себя и родственников. В промышленных масштабах сделать такое масло невозможно. В масле этом очень низкое содержание холестерина. Жир мелкодисперсный, в отличие от коровьего, поэтому цвет масла идеально белый. Рекомендовано нутрициологами как диетический продукт для всех возрастов потребителей.'),
(20, 11, 'yes', '2013-04-15', '2013-11-14', 'prostokvasha-iz-kozygo-moloka', 'Простокваша из козьего молока', 150, 170, '1 л', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком.'),
(21, 11, 'yes', '2013-04-15', '2013-11-14', 'sivorotka-iz-kozygo-moloka', 'Сыворотка из козьего молока', 45, 65, '1 л', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком.'),
(22, -1, 'no', '2013-04-15', '2013-11-22', 'sir-molodoy-prsniy-iz-kozygo-70-i-korovygo-30-moloka', 'Сыр молодой пресный из козьего 70% и коровьего 30% молока', 80, 95, '100 гр', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. Молодой пресный сыр очень рекомендуемый продукт для завтраков для взрослых и детей. Приготовленный из козьего молока считается более диетическим и полезным для взрослых. В смешанном варианте больше козьего, коровье добавляется для сливочного вкуса сыра.'),
(23, -1, 'no', '2013-04-15', '2013-11-22', 'sir-tvrdiy-iz-smsi-kozygo-70-i-korovygo-30-moloka', 'Сыр твердый из смеси козьего 70% и коровьего 30% молока', 85, 99, '100 гр', 'Наш твёрдый сыр делается как из чисто козьего, так и из смеси козьего с коровьим молока в пропорции 70/30. В данный рецепт авторского твёрдого сыра, в результате многомесячных испытаний мы стали добавлять коровье молоко от Джерсийской коровы высокой жирности для придания сыру приятного сливочного оттенка вкуса продукта.'),
(24, -1, 'no', '2013-04-15', '2013-11-22', 'sir-tipa-chevre-iz-smsi-kozygo-70-i-korovygo-30-moloka', 'Сыр типа Chevre из смеси козьего 70% и коровьего 30% молока', 70, 85, '100 гр', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. Сыр типа Шевре делается по рецепту, привезённому из Франции. Коровье молоко добавляется для придания сыру сливочного вкуса. По заказу можем сделать Шевре с укропом или с базиликом. Идеальный завтрак.'),
(25, 9, 'yes', '2013-04-15', '2013-04-30', 'korovy-moloko-elitno-ot-dzhrsiyskoy-korovi', 'Коровье молоко элитное от джерсийской коровы', 100, 150, '1 л', 'Пасутся наши коровы на заливных лугах р.Дубна, в лесу, сено на зиму готовим сами. В рационе наших коров только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается материнским молоком. Джерсийская корова даёт молоко 6-7% жирности и с высоким содержанием белка, что очень подходит для производства сыров. В частности, сыра типа камамбер с белой плесенью.'),
(26, 9, 'yes', '2013-04-15', '2013-04-22', 'korovy-moloko', 'Коровье молоко ', 90, 110, '1 л', 'Пасутся наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и на зиму готовим сами. В рационе наших коров только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается материнским молоком. Жирность молока натуральная, без добавления пальмового масла. Молоко цельное, не сепарированное. Можем пастеризовать или нет, по желанию клиента.'),
(27, 7, 'yes', '2013-04-15', '2013-11-14', 'tvorog-iz-korovygo-moloka', 'Творог из коровьего молока', 40, 50, '100 гр', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком.'),
(29, 7, 'yes', '2013-04-15', '2013-04-18', 'prostokvasha-iz-korovygo-moloka', 'Простокваша из коровьего молока', 120, 140, '1 л', 'Пасутся козы и наши 2 коровы на заливных лугах р.Дубна, в лесу, сено и веники для коз на зиму готовим сами. В рационе наших коз только натуральные эко-корма, такие как элитное сено, круглый год сочные корма, молодняк отпаивается козьим молоком. '),
(30, 7, 'yes', '2013-04-15', '2013-04-30', 'sivorotka-iz-korovygo-moloka', 'Сыворотка из коровьего молока', 35, 45, '1 л', 'Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока Сыворотка из коровьего молока'),
(31, 6, 'yes', '2013-04-15', '2013-11-14', 'ytsa-kurini', 'Яйца куриные', 100, 100, '1 десяток', 'Куры свободного выпаса. Наши куры питаются только эко-кормами и все лето свободно пасутся на лужайке. Яйца от них, красные и белые, очень вкусные.'),
(32, 6, 'no', '2013-04-15', '2013-11-22', 'ytsa-prplini', 'Яйца перепелиные', 120, 140, '1 десяток', 'Перепелиные яйца – настоящая биологически активная добавка, полная витаминов. Несмотря на свой крохотный размер (вес одного яйца всего 10-12 г), перепелиные яйца дадут фору куриным по содержанию витаминов и других полезных веществ: витамина А в них больше в 2,5 раза, В1 – в 2,8 и В2 – в 2,2 раза. Помимо этого, витамин D находится в активной форме, препятствуя развитию рахита у детей.'),
(33, 6, 'no', '2013-04-15', '2013-11-22', 'myaso-kozlikov-ot-polu-tushki-kogda-sty-v-nalichii', 'Мясо козликов (от полу-тушки), когда есть в наличии', 45, 65, '100 гр', 'Мы реализует только мясо молочных козликов. Именно поэтому это мясо имеет нежный и тонкий вкус и аромат, без никаких посторонних ароматов. Мясо молодых козликов предпочитали римские патриции и современные олигархи. Даже в Европе, запечённая в печи на дровах ножка молочного козлика большая редкость и дорогое удовольствие. Продаем это эко-мясо ОТ полутушки. Вес полутушки примерно от 3-х до 3,5 кг. Мясо молодых(до 1,5 месяцев) молочных козликов есть редкий диетический элитный продукт. Мясо это очень нежное и почти не содержит холестерина, поэтому рекомендовано также пожилым людям и людям в послеоперационном периоде.'),
(34, -1, 'no', '2013-04-22', '2013-11-22', 'sir-s-bloy-plsnyyu-tipa-kamambr-iz-smsi-kozygo-70-i-korovygo-30-molokaizgotovlni-po-zakazu', 'Сыр с белой плесенью типа камамбер из смеси козьего 70% и коровьего 30% молока(изготовление по заказу)', 95, 115, '100 гр', 'Наш сыр, типа камамбер, с белой плесенью, есть предмет особой гордости автора всех молочных продуктов фермы Коза Ностра. Изготавливается он совершенно отдельно от всех остальных сыров и молока вообще, т.к. используемая для его приготовления белая плесень Penicillium Candidum и конкретно её споры могут попасть на другие молочные продукты. Плесень эту мы привозим из Франции. Выдерживается сыр в специальном шкафу при постоянной температуре 12 градусов и влажности 90%. Срок вызревания от 3-х до 4-х недель в зависимости от состава молока и времени года.'),
(35, 6, 'no', '2013-05-13', '2013-11-22', 'pchny-i-srdts-molochnih-kozlikov', 'Печень и сердце молочных козликов', 600, 600, '100 гр', 'Козлята выращиваются в экологически чистом районе на ферме &quot;Коза Ностра&quot;. В первые 8 недель козлята питаются исключительно молоком. Козлята выращиваются до 1,5-2 месячного возраста.'),
(36, 6, 'yes', '2013-05-13', '2013-11-14', 'supovi-kurikogda-sty-v-nalichi', 'Суповые куры(когда есть в наличие)', 50, 50, '100 гр', 'Куры свободного выпаса. Наши куры питаются только эко-кормами и все лето свободно пасутся на лужайке. Яйца от них, красные и белые, очень вкусные.'),
(37, 11, 'yes', '2013-12-02', '2013-12-05', 'molodoy-koziy-sir-tipa-shvre-s-olivkamidlya-namazivaniya-za-zavtrakom', 'Молодой козий сыр типа Шеврэ с оливками(для намазывания за завтраком)', 85, 98, '100 гр', 'К Новому году мы выпускаем в продажу новый вид сыра: мягкий козий сыр Шевр с маслинами! Работа по созданию этого нового продукта заняла несколько месяцев. Пришлось привлечь к созданию нового продукта испанских коллег, больших знатоков средиземноморской диеты и сырного производства. Испытывались варианты сыра как с зелёными оливками, так и с чёрными разных сортов. По физико-химическим свойствам и по органолептике победило сочетание нашего варианта мягкого Шевра с маслинами сорта касеренья. Предназначен сыр для поедания за завтраком, намазанный на хрустящий белого хлеба тост. На Российском рынке аналогов нет!');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `inserted_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `slug` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `inserted_date`, `updated_date`, `slug`, `tipo`) VALUES
(1, '2013-03-08 11:27:49', '2013-03-08 11:27:49', 'Slug', 'Tipo'),
(2, '2013-03-08 11:28:00', '2013-03-08 11:28:16', 'Slug1', 'Tipo1'),
(4, '2013-03-08 14:52:55', '2013-03-08 14:52:55', 'slug2', 'Tipo'),
(5, '2013-03-08 14:54:34', '2013-03-08 14:54:34', 'slug-asda-asdasd', 'Tipo'),
(6, '2013-03-08 14:54:53', '2013-03-08 14:55:10', 'slug-asd-asdasd', 'Tipo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inserted_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_address` (`email_address`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `inserted_date`, `updated_date`, `username`, `password`, `email_address`) VALUES
(103, '0000-00-00', '0000-00-00', 'antonrodin', 'dc2632e868a4941f7ff72b308a15dc07', 'antonzekeriev@gmail.com'),
(104, '0000-00-00', '0000-00-00', 'administrator', '846163732f77a2740e3f283525af0a19', 'admin@renovarcarnet.com'),
(105, '2013-04-05', '2013-04-05', 'Username', 'Password', 'Email Address'),
(106, '2013-04-30', '2013-04-30', 'aquesolo', '381612b967440d9aa82eb0dd18ee4555', 'andres@aquesolo.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
